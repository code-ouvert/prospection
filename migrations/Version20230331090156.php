<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230331090156 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE [AGENCE] ([AGENCE_ID] INT IDENTITY NOT NULL, PRIMARY KEY ([AGENCE_ID]))');
        $this->addSql('CREATE TABLE [COMMENTAIRE] ([COMMENTAIRE_ID] INT IDENTITY NOT NULL, [VALEUR] NVARCHAR(500), [USERCREATION] NVARCHAR(50) NOT NULL, [DATECREATION] DATETIME2(6) NOT NULL, [USERMODIFICATION] NVARCHAR(50), [DATEMODIFICATION] DATETIME2(6), PRIMARY KEY ([COMMENTAIRE_ID]))');
        $this->addSql('CREATE TABLE [DEPARTEMENT] ([DEPARTEMENT_ID] INT IDENTITY NOT NULL, [CODEDEPARTEMENT] NVARCHAR(100), [DEPARTEMENT] NVARCHAR(50), [DEPARTEMENT_SEARCH] NVARCHAR(50), PRIMARY KEY ([DEPARTEMENT_ID]))');
        $this->addSql('CREATE TABLE [OBJETAPPLI] ([OBJET_ID] INT IDENTITY NOT NULL, [NOM] NVARCHAR(100), PRIMARY KEY ([OBJET_ID]))');
        $this->addSql('CREATE TABLE [PARAMAPPLI] ([PARAMAPPLI_ID] INT IDENTITY NOT NULL, [CODE] NVARCHAR(100) NOT NULL, [ANNEE] NVARCHAR(100) NOT NULL, [CHIFFREAFFAIRE] NVARCHAR(1000), [FOND] NVARCHAR(1000), PRIMARY KEY ([PARAMAPPLI_ID]))');
        $this->addSql('CREATE TABLE [PARCELLE] ([PARCELLE_ID] INT IDENTITY NOT NULL, [PREFIX] NVARCHAR(50), [SECT] NVARCHAR(50), [NUMEROLOT] NVARCHAR(50), [SUPERFICIE] NUMERIC(10, 2), [INDIC] NVARCHAR(500), [NUMERO] NVARCHAR(50), [VOIE] NVARCHAR(100), [CODEPOSTAL] NVARCHAR(10), [VILLE] NVARCHAR(500), [Commentaire] NVARCHAR(1000), [FLAG_SUPPRESSION] NVARCHAR(2), PRIMARY KEY ([PARCELLE_ID]))');
        $this->addSql('CREATE TABLE [PROFIL] ([PROFIL_ID] INT IDENTITY NOT NULL, [LIBELLE] NVARCHAR(100), PRIMARY KEY ([PROFIL_ID]))');
        $this->addSql('CREATE TABLE [PROPRIETAIRE] ([PROPRIETAIRE_ID] INT IDENTITY NOT NULL, PRIMARY KEY ([PROPRIETAIRE_ID]))');
        $this->addSql('CREATE TABLE [REGION] ([REGION_ID] INT IDENTITY NOT NULL, [REGION] NVARCHAR(50), [REGION_SEARCH] NVARCHAR(50), PRIMARY KEY ([REGION_ID]))');
        $this->addSql('CREATE TABLE [TERRAIN] ([TERRAIN_ID] INT IDENTITY NOT NULL, PRIMARY KEY ([TERRAIN_ID]))');
        $this->addSql('CREATE TABLE [VILLE] ([VILLE_ID] INT IDENTITY NOT NULL, [CODEPOSTAL] NVARCHAR(10), [VILLE] NVARCHAR(50), [VILLE_SEARCH] NVARCHAR(50), [VILLE_SOUNDEX] NVARCHAR(4), PRIMARY KEY ([VILLE_ID]))');
        $this->addSql('CREATE TABLE [WEB_EDITION] ([WEB_EDITION_ID] INT IDENTITY NOT NULL, [DESIGNATION] NVARCHAR(255) NOT NULL, [TYPEEDITION] INT NOT NULL, [COMMENTAIRE] NVARCHAR(255), [CHEMIN] NVARCHAR(255) NOT NULL, PRIMARY KEY ([WEB_EDITION_ID]))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('DROP TABLE [AGENCE]');
        $this->addSql('DROP TABLE [COMMENTAIRE]');
        $this->addSql('DROP TABLE [DEPARTEMENT]');
        $this->addSql('DROP TABLE [OBJETAPPLI]');
        $this->addSql('DROP TABLE [PARAMAPPLI]');
        $this->addSql('DROP TABLE [PARCELLE]');
        $this->addSql('DROP TABLE [PROFIL]');
        $this->addSql('DROP TABLE [PROPRIETAIRE]');
        $this->addSql('DROP TABLE [REGION]');
        $this->addSql('DROP TABLE [TERRAIN]');
        $this->addSql('DROP TABLE [VILLE]');
        $this->addSql('DROP TABLE [WEB_EDITION]');
    }
}
