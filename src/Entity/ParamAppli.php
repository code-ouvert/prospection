<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ParamAppliRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ParamAppliRepository::class)]
#[ORM\Table(name: '`PARAMAPPLI`')]
#[ApiResource]
class ParamAppli
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`PARAMAPPLI_ID`')]
    private ?int $id = null;

    #[ORM\Column(
        name: '`CODE`',
        length: 100,
        nullable: false,
    )]
    private ?string $code = null;

    #[ORM\Column(
        name: '`ANNEE`',
        length: 100,
        nullable: false,
    )]
    private ?string $annee = null;

    #[ORM\Column(
        name: '`CHIFFREAFFAIRE`',
        length: 1000,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $chiffreAffaire = null;

    #[ORM\Column(
        name: '`FOND`',
        length: 1000,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $fond = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getAnnee(): ?string
    {
        return $this->annee;
    }

    public function setAnnee(?string $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getChiffreAffaire(): ?string
    {
        return $this->chiffreAffaire;
    }

    public function setChiffreAffaire(?string $chiffreAffaire): self
    {
        $this->chiffreAffaire = $chiffreAffaire;

        return $this;
    }

    public function getFond(): ?string
    {
        return $this->fond;
    }

    public function setFond(?string $fond): self
    {
        $this->fond = $fond;

        return $this;
    }
}
