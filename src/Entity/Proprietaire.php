<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ProprietaireRepository;

#[ORM\Entity(repositoryClass: ProprietaireRepository::class)]
#[ORM\Table(name: '`PROPRIETAIRE`')]
#[ApiResource]
class Proprietaire
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`PROPRIETAIRE_ID`')]
    private ?int $id = null;

    public function getId(): ?int
    {
        return $this->id;
    }
}
