<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\WebEditionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WebEditionRepository::class)]
#[ORM\Table(name: '`WEB_EDITION`')]
#[ApiResource]
class WebEdition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`WEB_EDITION_ID`')]
    private ?int $id = null;

    #[ORM\Column(
        name: '`DESIGNATION`',
        length: 255,
        nullable: false,
    )]
    private ?string $designation = null;

    #[ORM\Column(
        name: '`TYPEEDITION`',
        nullable: false,
    )]
    private ?int $typeEdition = null;

    #[ORM\Column(
        name: '`COMMENTAIRE`',
        length: 255,
        nullable: true,
    )]
    private ?string $commentaire = null;

    #[ORM\Column(
        name: '`CHEMIN`',
        length: 255,
        nullable: false,
    )]
    private ?string $chemin = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(?string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getTypeEdition(): ?int
    {
        return $this->typeEdition;
    }

    public function setTypeEdition(?int $typeEdition): self
    {
        $this->typeEdition = $typeEdition;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    public function setChemin(?string $chemin): self
    {
        $this->chemin = $chemin;

        return $this;
    }
}
