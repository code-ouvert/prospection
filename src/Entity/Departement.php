<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\DepartementRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DepartementRepository::class)]
#[ORM\Table(name: '`DEPARTEMENT`')]
#[ApiResource]
class Departement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`DEPARTEMENT_ID`')]
    private ?int $id = null;

    #[ORM\Column(
        name: '`CODEDEPARTEMENT`',
        length: 100,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $codeDepartement = null;

    #[ORM\Column(
        name: '`DEPARTEMENT`',
        length: 50,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $departement = null;

    #[ORM\Column(
        name: '`DEPARTEMENT_SEARCH`',
        length: 50,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $departementSearch = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeDepartement(): ?string
    {
        return $this->codeDepartement;
    }

    public function setCodeDepartement(?string $codeDepartement): self
    {
        $this->codeDepartement = $codeDepartement;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(?string $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getDepartementSearch(): ?string
    {
        return $this->departementSearch;
    }

    public function setDepartementSearch(?string $departementSearch): self
    {
        $this->departementSearch = $departementSearch;

        return $this;
    }
}
