<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\TerrainRepository;

#[ORM\Entity(repositoryClass: TerrainRepository::class)]
#[ORM\Table(name: '`TERRAIN`')]
#[ApiResource]
class Terrain
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`TERRAIN_ID`')]
    private ?int $id = null;

    public function getId(): ?int
    {
        return $this->id;
    }
}
