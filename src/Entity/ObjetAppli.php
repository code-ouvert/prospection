<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ObjetAppliRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ObjetAppliRepository::class)]
#[ORM\Table(name: '`OBJETAPPLI`')]
#[ApiResource]
class ObjetAppli
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`OBJET_ID`')]
    private ?int $id = null;

    #[ORM\Column(
        name: '`NOM`',
        length: 100,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $nom = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
}
