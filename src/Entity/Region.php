<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\RegionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RegionRepository::class)]
#[ORM\Table(name: '`REGION`')]
#[ApiResource]
class Region
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`REGION_ID`')]
    private ?int $id = null;

    #[ORM\Column(
        name: '`REGION`',
        length: 50,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $region = null;

    #[ORM\Column(
        name: '`REGION_SEARCH`',
        length: 50,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $regionSearch = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getRegionSearch(): ?string
    {
        return $this->regionSearch;
    }

    public function setRegionSearch(?string $regionSearch): self
    {
        $this->regionSearch = $regionSearch;

        return $this;
    }
}
