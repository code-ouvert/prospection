<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\AgenceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AgenceRepository::class)]
#[ORM\Table(name: '`AGENCE`')]
#[ApiResource]
class Agence
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`AGENCE_ID`')]
    private ?int $id = null;

    public function getId(): ?int
    {
        return $this->id;
    }
}
