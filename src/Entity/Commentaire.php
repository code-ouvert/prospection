<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\CommentaireRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommentaireRepository::class)]
#[ORM\Table(name: '`COMMENTAIRE`')]
#[ApiResource]
class Commentaire
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`COMMENTAIRE_ID`')]
    private ?int $id = null;

    #[ORM\Column(
        name: '`VALEUR`',
        length: 500,
        nullable: true,
    )]
    private ?string $valeur = null;

    #[ORM\Column(
        name: '`USERCREATION`',
        length: 50,
        nullable: false,
    )]
    private ?string $userCreation = null;

    #[ORM\Column(
        name: '`DATECREATION`',
        type: Types::DATETIME_MUTABLE,
        nullable: false,
    )]
    private ?\DateTimeInterface $dateCreation = null;

    #[ORM\Column(
        name: '`USERMODIFICATION`',
        length: 50,
        nullable: true,
    )]
    private ?string $userModification = null;

    #[ORM\Column(
        name: '`DATEMODIFICATION`',
        type: Types::DATETIME_MUTABLE,
        nullable: true,
    )]
    private ?\DateTimeInterface $dateModification = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValeur(): ?string
    {
        return $this->valeur;
    }

    public function setValeur(?string $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getUserCreation(): ?string
    {
        return $this->userCreation;
    }

    public function setUserCreation(?string $userCreation): self
    {
        $this->userCreation = $userCreation;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getUserModification(): ?string
    {
        return $this->userModification;
    }

    public function setUserModification(?string $userModification): self
    {
        $this->userModification = $userModification;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(?\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }
}
