<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ParcelleRepository;

#[ORM\Entity(repositoryClass: ParcelleRepository::class)]
#[ORM\Table(name: '`PARCELLE`')]
#[ApiResource]
class Parcelle
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`PARCELLE_ID`')]
    private ?int $id = null;

    #[ORM\Column(
        name: '`PREFIX`',
        length: 50,
        nullable: true,
    )]
    private ?string $prefix = null;

    #[ORM\Column(
        name: '`SECT`',
        length: 50,
        nullable: true,
    )]
    private ?string $sect = null;

    #[ORM\Column(
        name: '`NUMEROLOT`',
        length: 50,
        nullable: true,
    )]
    private ?string $numeroLot = null;

    #[ORM\Column(
        name: '`SUPERFICIE`',
        type: Types::DECIMAL,
        precision: 10,
        scale: '2',
        nullable: true,
    )]
    private ?string $superficie = null;

    #[ORM\Column(
        name: '`INDIC`',
        length: 500,
        nullable: true,
    )]
    private ?string $indic = null;

    #[ORM\Column(
        name: '`NUMERO`',
        length: 50,
        nullable: true,
    )]
    private ?string $numero = null;

    #[ORM\Column(
        name: '`VOIE`',
        length: 100,
        nullable: true,
    )]
    private ?string $voie = null;

    #[ORM\Column(
        name: '`CODEPOSTAL`',
        length: 10,
        nullable: true,
    )]
    private ?string $codePostal = null;

    #[ORM\Column(
        name: '`VILLE`',
        length: 500,
        nullable: true,
    )]
    private ?string $ville = null;

    #[ORM\Column(
        name: '`Commentaire`',
        length: 1000,
        nullable: true,
    )]
    private ?string $commantaire = null;

    #[ORM\Column(
        name: '`FLAG_SUPPRESSION`',
        length: 2,
        nullable: true,
    )]
    private ?string $flagSuppression = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrefix(): ?string
    {
        return $this->prefix;
    }

    public function setPrefix(?string $prefix): self
    {
        $this->prefix = $prefix;

        return $this;
    }

    public function getSect(): ?string
    {
        return $this->sect;
    }

    public function setSect(?string $sect): self
    {
        $this->sect = $sect;

        return $this;
    }

    public function getNumeroLot(): ?string
    {
        return $this->numeroLot;
    }

    public function setNumeroLot(?string $numeroLot): self
    {
        $this->numeroLot = $numeroLot;

        return $this;
    }

    public function getSuperficie(): ?string
    {
        return $this->superficie;
    }

    public function setSuperficie(?string $superficie): self
    {
        $this->superficie = $superficie;

        return $this;
    }

    public function getIndic(): ?string
    {
        return $this->indic;
    }

    public function setIndic(?string $indic): self
    {
        $this->indic = $indic;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getVoie(): ?string
    {
        return $this->voie;
    }

    public function setVoie(?string $voie): self
    {
        $this->voie = $voie;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCommantaire(): ?string
    {
        return $this->commantaire;
    }

    public function setCommantaire(?string $commantaire): self
    {
        $this->commantaire = $commantaire;

        return $this;
    }

    public function getFlagSuppression(): ?string
    {
        return $this->flagSuppression;
    }

    public function setFlagSuppression(?string $flagSuppression): self
    {
        $this->flagSuppression = $flagSuppression;

        return $this;
    }
}
