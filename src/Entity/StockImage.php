<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\StockImageRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StockImageRepository::class)]
#[ORM\Table(name: '`stockImage`')]
#[ApiResource]
class StockImage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`pkImage`')]
    private ?int $id = null;

    #[ORM\Column(
        name: '`nomImage`',
        length: 50,
        nullable: false,
    )]
    private ?string $nomImage = null;

    #[ORM\Column(
        name: '`mimeType`',
        length: 50,
        nullable: false,
    )]
    private ?string $mimeType = null;

    #[ORM\Column(
        name: '`extension`',
        length: 50,
        nullable: true,
    )]
    private ?string $extension = null;

    #[ORM\Column(
        name: '`imageStocke`',
        length: 255,
        nullable: false,
    )]
    private ?string $imageStocke = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomImage(): ?string
    {
        return $this->nomImage;
    }

    public function setNomImage(?string $nomImage): self
    {
        $this->nomImage = $nomImage;

        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(?string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setExtension(?string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getImageStocke(): ?string
    {
        return $this->imageStocke;
    }

    public function setImageStocke(?string $imageStocke): self
    {
        $this->imageStocke = $imageStocke;

        return $this;
    }
}
