<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\VilleRepository;

#[ORM\Entity(repositoryClass: VilleRepository::class)]
#[ORM\Table(name: '`VILLE`')]
#[ApiResource]
class Ville
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: '`VILLE_ID`')]
    private ?int $id = null;

    #[ORM\Column(
        name: '`CODEPOSTAL`',
        length: 10,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $codePostal = null;

    #[ORM\Column(
        name: '`VILLE`',
        length: 50,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $ville = null;

    #[ORM\Column(
        name: '`VILLE_SEARCH`',
        length: 50,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $villeSearch = null;

    #[ORM\Column(
        name: '`VILLE_SOUNDEX`',
        length: 4,
        nullable: true,
        options: ["default" => null],
    )]
    private ?string $villeSoundex = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getVilleSearch(): ?string
    {
        return $this->villeSearch;
    }

    public function setVilleSearch(?string $villeSearch): self
    {
        $this->villeSearch = $villeSearch;

        return $this;
    }

    public function getVilleSoundex(): ?string
    {
        return $this->villeSoundex;
    }

    public function setVilleSoundex(?string $villeSoundex): self
    {
        $this->villeSoundex = $villeSoundex;

        return $this;
    }
}
